﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzTests
{
    [TestClass]
    public class FizzBuzzTests
    {
        [TestMethod]
        [DataRow(3)]
        [DataRow(6)]
        [DataRow(9)]
        public void FizzBuzz_GetOutput_ReturnsFizz_WhenNumberIsDivisbleBy_Three(int number)
        {
            string expectedString = "Fizz";
            Assert.AreEqual(FizzBuzz.FizzBuzz.GetOutput(number), expectedString);
        }

        [TestMethod]
        [DataRow(5)]
        [DataRow(10)]
        [DataRow(25)]
        public void FizzBuzz_GetOutput_ReturnsBuzz_WhenNumberIsDivisbleBy_Five(int number)
        {
            string expectedString = "Buzz";
            Assert.AreEqual(FizzBuzz.FizzBuzz.GetOutput(number), expectedString);
        }

        [TestMethod]
        [DataRow(15)]
        [DataRow(45)]
        [DataRow(90)]
        public void FizzBuzz_GetOutput_ReturnsFizzBuzz_WhenNumberIsDivisbleBy_ThreeAndFive(int number)
        {
            string expectedString = "FizzBuzz";
            Assert.AreEqual(FizzBuzz.FizzBuzz.GetOutput(number), expectedString);
        }

        [TestMethod]
        [DataRow(2)]
        [DataRow(8)]
        [DataRow(47)]
        public void FizzBuzz_GetOutput_ReturnsSameNumber_WhenNumberIsNotDivisbleBy_ThreeAndFive(int number)
        {            
            Assert.AreEqual(FizzBuzz.FizzBuzz.GetOutput(number), number.ToString());
        }
    }
}

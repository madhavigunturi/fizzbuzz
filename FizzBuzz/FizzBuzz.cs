﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz
{
   public class FizzBuzz
    {
        /// <summary>
        /// Returns :
        /// "Fizz"  if number is divisble by three
        /// "Buzz" if number is divisble by five
        /// "FuzzBuzz" if number is divible by three and five
        /// the input number if not divisible by three or five        
        /// </summary>
        public static string GetOutput(int input)
        {
            var rules = new List<Tuple<int, string>>()
            {
                new Tuple<int, string>(3, "Fizz"),
                new Tuple<int, string>(5, "Buzz")
            };

            bool isMatch(int number, int rule) => number % rule == 0;


            var matchingRules = rules.Where(c => isMatch(input, c.Item1)).ToList();
            if (matchingRules.Any())
            {
                return string.Join("", matchingRules.Select(c => c.Item2));
            }
            else
            {
                return input.ToString();
            }
        }
    }
}

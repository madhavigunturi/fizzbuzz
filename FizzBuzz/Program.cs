﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            // Print FizzBuzz number output for values 1 to 100.          
            for(int number = 1; number <=100; number++)
            {
                Console.WriteLine(FizzBuzz.GetOutput(number));
            }

            Console.WriteLine("Press enter to quit...");
            Console.ReadLine();
        }      
    }
}
